<?php
/**
 * @file
 * Module API, implementing custom hooks.
 */

/**
 * hook_wss_base_register_service().
 *
 * Return an array of wss services to register.
 */
function hook_wss_base_register_service() {
  $services = array();

  $services['sodis'] = array(
    'title' => t('SODIS'),
    'service class' => '\publicplan\wss\sodis\Service',
  );

  return $services;
}
