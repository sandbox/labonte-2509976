<?php // $Id$
/**
 * @file
 * Module's admin settings.
 */
use publicplan\wss\base\SearchService;

/**
 * Callback function for an overview of all WSS specific administration pages.
 *
 * @return array#
 *   Renderable content.
 */
function wss_base_conf_menu_block_page() {
  $item = menu_get_item('admin/config/services/wss');
  if (($content = system_admin_menu_block($item))) {
    $output = theme('admin_block_content', array('content' => $content));
  }
  else {
    $output = t('It seems that you do not have sufficient rights to access these pages.');
  }

  return $output;
}

/**
 * Callback function for common administration settings.
 *
 * @return array
 *   Renderable form.
 */
function wss_base_admin_settings() {
  $settings = array();

  $settings[SearchService::SETTING__PERSISTENT_SETTINGS] = array(
    '#type' => 'checkbox',
    '#title' => t('Keep settings persistent'),
    '#description' => t("Usually when uninstalling a module, all associated configuration
      variables will be removed from database. Activating this setting assures that configrations
      of the WebService Search modules will be kept in database. This feature will be most useful
      for development reasons. Don't mind it, if you do not know, what it should be useful for."),
    '#default_value' => variable_get(SearchService::SETTING__PERSISTENT_SETTINGS),
  );

  return system_settings_form($settings);
}

/**
 * Implements a custom submit handler.
 */
function wss_base_admin_settings_submit($form, &$form_state) {
  if (isset($form_state['values']) && !empty($form_state['values'])) {
    cache_clear_all();
    drupal_set_message('Cleared all caches.');
  }
}
