
-- SUMMARY --

The Webservice Search Base module (ws_search_base) provides some basic classes
and their corresponding interfaces for implementing .

For a full description of the module, visit the project page:
  http://drupal.org/project/admin_menu

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/admin_menu


-- REQUIREMENTS --

None. But this module at its own won't make any sense. It's just the common
basis for some additional modules, that can be used to search through arbitrary
web services of the german educational system.

Of course you can also use it as basis for your own module, when communicating
with foreign web services.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --


-- CUSTOMIZATION --


-- TROUBLESHOOTING --


-- CONTACT --

Current maintainers:
* Yannic Labonte (labonte) - http://drupal.org/user/54136

This project has been sponsored by:
* publicplan
  publicplan actively pushes the use of Drupal in the public sector by
  introducing it in consulting projects as well as software and web portal
  development.

* German Ministry of Education in North Rhine-Westphalia
  Visit http://www.learnline.schulministerium.nrw.de or
  http://schulministerium.nrw.de for more information.
