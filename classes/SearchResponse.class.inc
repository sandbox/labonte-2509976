<?php
// $id:$

/**
 * @file
 * This file contains the SearchResultBase class implementing the SearchResult
 * interface.
 */

namespace publicplan\wss\base;

abstract class SearchResponse implements ResponseInterface {
  /**
   * Service identifier.
   *
   * @var string
   */
  protected $service;

  /**
   * Originating request.
   *
   * @var \publicplan\wss\base\SearchRequest
   */
  protected $request;

  /**
   * Untouched response body.
   *
   * @var string
   */
  protected $body = '';

  /**
   * Response data portion as probably Array, ArrayObject or SimpleXMLElement.
   *
   * @var mixed
   */
  protected $nativeResponse = NULL;

  /**
   * Parsed response data not related to a single result.
   *
   * @var array
   */
  protected $data = array();

  /**
   * Array of results.
   *
   * @var \publicplan\wss\base\SearchResult[]
   */
  protected $results = array();

  /**
   * Object instances for clone based object initialization.
   *
   * @var \publicplan\wss\base\SearchResponse[]
   */
  protected static $objects = array();

  /**
   * Parse response data.
   *
   * This method should parse response data which is not part of a single result
   * unit, eg. the total result count.
   *
   * @return \publicplan\wss\base\Response
   *   Returns itself.
   */
  abstract protected function parseResponseData();

  /**
   * Extract and return the search results.
   *
   * This method should return an array or iterable object of single result data
   * which will then be parsed using the parse method of the Result class.
   *
   * @return array
   *   Array or iterable object of result data units.
   */
  abstract protected function extractResults();

  /**
   * Contructor.
   */
  protected function __construct() {}

  /**
   * Create a new object of this class.
   *
   * @return \publicplan\wss\base\SearchResponse
   *   New object instance of this class.
   */
  public static function create() {
    $class_name = get_called_class();
    if (!isset(self::$objects[$class_name])) {
      self::$objects[$class_name] = new $class_name();
    }

    $new_object = clone self::$objects[$class_name];

    return $new_object;
  }

  /**
   * Parse raw result data.
   *
   * @param \publicplan\wss\base\SearchRequest $request
   *   The Request object to parse.
   * @param object $response
   *   Native response object.
   *
   * @return \publicplan\wss\base\SearchResponse
   *   Returns itself.
   */
  public function parse(\publicplan\wss\base\SearchRequest $request, $response) {
    $this->request = $request;
    $this->nativeResponse = $response;
    $this->body = $this->nativeResponse->data;

    if (!$this->getError()) {
      $this->parseResponseData()->parseResults();
    }

    return $this;
  }

  /**
   * Parse results using the Result class' parse() method.
   *
   * @return \publicplan\wss\base\SearchResponse
   *   Returns itself.
   */
  protected function parseResults() {
    $object_factory = '\publicplan\wss\\' . $this->service . '\Result::create';
    foreach ($this->extractResults() as $raw_result_data) {
      $result = call_user_func($object_factory);
      $this->results[] = $result->setResponse($this)->parse($raw_result_data);
    }

    return $this;
  }

  /**
   * Implements Countable.
   *
   * @return int
   *   Number of results.
   */
  public function count() {
    return count($this->results);
  }

  /**
   * Implements IteratorAggregate.
   *
   * @return \ArrayIterator
   *   \ArrayIterator object of the resultset.
   */
  public function getIterator() {
    return new \ArrayIterator($this->results);
  }

  /**
   * Get the originating HTTP request as raw string.
   *
   * @return string|FALSE
   *   The raw request string.
   */
  public function getRequestString() {
    return isset($this->nativeResponse) && isset($this->nativeResponse->request) ?
      $this->nativeResponse->request : FALSE;
  }

  /**
   * Get HTTP response code.
   *
   * @return int
   *   HTTP status code.
   */
  public function getStatusCode() {
    return $this->nativeResponse->code;
  }

  /**
   * Get the HTTP status message.
   *
   * @return string
   *   HTTP status message.
   */
  public function getStatusMessage() {
    return isset($this->nativeResponse->error) ?
      $this->nativeResponse->error : $this->nativeResponse->status_message;
  }

  /**
   * Get HTTP error message or FALSE if no error occurred.
   *
   * @return string|FALSE
   *   HTTP status message in case of an error.
   */
  public function getError() {
    return isset($this->nativeResponse->error) ?
      $this->nativeResponse->error : FALSE;
  }

  /**
   * Get the HTTP version string e.g. 'HTTP/1.1'.
   *
   * @return string|FALSE
   *   HTTP version string.
   */
  public function getVersion() {
    return isset($this->nativeResponse->protocol) ?
      $this->nativeResponse->protocol : FALSE;
  }

  /**
   * Get the initial HTTP status code, if the request was redirected.
   *
   * @return int|FALSE
   *   Original HTTP status code in case of a redirect.
   */
  public function getRedirectStatusCode() {
    return isset($this->nativeResponse->redirect_code) ?
      $this->nativeResponse->redirect_code : FALSE;
  }

  /**
   * Get the target's URL string in case of a redirect; otherwise FALSE.
   *
   * @return string|FALSE
   *   Value of the 'Location' header if applicable.
   */
  public function getRedirectUrl() {
    return isset($this->nativeResponse->redirect_url) ?
      $this->nativeResponse->redirect_url : FALSE;
  }

  /**
   * Get the HTTP response headers as associative array.
   *
   * @return array|FALSE
   *   Response headers as associative array.
   */
  public function getHeaders() {
    return isset($this->nativeResponse->headers) ?
      $this->nativeResponse->headers : FALSE;
  }

  /**
   * Returns the raw response body.
   *
   * @return string|FALSE
   *   Raw response body.
   */
  public function getRawData() {
    return isset($this->nativeResponse->data) ?
      $this->nativeResponse->data : FALSE;
  }

  /**
   * Returns an appropriate PHP representation for response data.
   *
   * Data format depends on the service and should be in an appropriate PHP
   * representation for the raw response format.
   * (eg. JSON -> Array/ArrayObject, XML -> SimpleXMLElement)
   *
   * @return mixed
   *   Body data. Type may vary based on raw type (JSON -> Array/ArrayObject;
   *   XML -> SimpleXMLElement).
   */
  public function getData($key = NULL) {
    if (!isset($key)) {
      return $this->data;
    }

    return array_key_exists($key, $this->data) ? $this->data[$key] : NULL;
  }

  /**
   * Returns an array containing all results.
   *
   * Array keys are the appropriate result IDs.
   *
   * @return \publicplan\wss\base\SearchResult[]
   *   The actual results.
   */
  public function getResults() {
    return $this->results;
  }

  /**
   * Return the result data as array structure.
   *
   * @return array
   *   Array of result data arrays.
   */
  public function getResultData() {
    $data_extract_func = function($r) {
      return $r->getData();
    };
    $result_data = \array_map($data_extract_func, $this->getResults());

    return $result_data;
  }

  /**
   * Returns the result specified by its index.
   *
   * @param string $id
   *   Result ID.
   *
   * @return \publicplan\wss\base\SearchResult
   *   The search result with ID $index.
   */
  public function getResult($id) {
    if (array_key_exists($id, $this->results)) {
      return $this->results[$id];
    }
    else {
      throw new \RuntimeException(
      t("This SearchResponse contains no result with ID '@id'.", array(
        '@id' => $id)));
    }
  }

  /**
   * Return the last received result.
   *
   * @return \publicplan\wss\base\Result
   *   The most recent result.
   *
   * @throws \RuntimeException
   */
  public static function getLast() {
    $class = get_called_class();
    if (!isset(self::$instance[$class])) {
      throw new \RuntimeException(t('No Response received/built yet.'));
    }

    return self::$instance[$class];
  }

  /**
   * Implements ArrayAccess interface.
   *
   * @param mixed $offset
   *   Array index.
   *
   * @return bool
   *   Return whether the given index exists.
   */
  public function offsetExists($offset) {
    return array_key_exists($offset, $this->results);
  }

  /**
   * Implements ArrayAccess interface.
   *
   * @param mixed $offset
   *   Index for which to return the value.
   *
   * @return mixed
   *   The value.
   */
  public function offsetGet($offset) {
    return array_key_exists($offset, $this->results) ?
      $this->results[$offset] : NULL;
  }

  /**
   * Implements ArrayAccess interface.
   *
   * @param mixed $offset
   *   Index/Key to set value for.
   *
   * @param mixed $value
   *   Value to be set.
   */
  public function offsetSet($offset, $value) {
    if (!isset($offset)) {
      $this->results[] = $value;
    }
    else {
      $this->results[$offset] = $value;
    }
  }

  /**
   * Implements ArrayAccess interface.
   *
   * @param mixed $offset
   *   Index/Key of the value that should be unset.
   */
  public function offsetUnset($offset) {
    if (array_key_exists($offset, $this->results)) {
      unset($this->results[$offset]);
    }
  }
}
