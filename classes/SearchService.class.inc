<?php // $Id$

/**
 * @file
 * Main module file.
 */

namespace publicplan\wss\base;

abstract class SearchService implements ServiceInterface {

  /**
   * Bitwise arguments used to indicate, which
   * backend service(s) to use for search.
   */
  const SEARCH_ALL = 0xff;
  const SEARCH_SODIS = 0x01;
  const SEARCH_EDMOND = 0x02;

  /**
   * Recurring array keys.
   */
  const ACTIVE = '#active';
  const AVAILABLE = '#available';
  const DISPLAY_VALUE = '#display_value';
  const VALUES = '#values';
  const VALUE = '#value';
  const COUNT = '#count';
  const TITLE = '#title';
  const IS_ARRAY = '#is_array';
  const HREF = '#href';

  /**
   * Recurring array keys (espacially for parameter mapping).
   */
  const FILTER = 'filter';
  const SODIS = 'sodis';
  const EDMOND = 'edmond';
  const EDUTAGS = 'edutags';
  const PARAM = 'parameter';
  const HIDDEN = 'hidden';

  /**
   * Valid input parameter names.
   * Used for parameter mapping.
   *
   * @see $parameter_mapping
   * @see __construct()
   */
  const PARAM__SEARCH = 'search';
  const PARAM__EDMOND = 'edmond';
  const PARAM__PAGE = 'page';
  const PARAM__SIZE = 'size';
  const PARAM__FROM = 'from';
  const PARAM__SORT = 'sort';
  const PARAM__ORDER = 'order';
  const PARAM__KEYWORDS = 'keywords';
  const PARAM__LANGUAGES = 'languages';
  const PARAM__CONTEXTS = 'contexts';
  const PARAM__DISCIPLINES = 'disciplines';
  const PARAM__CONTENTTYPES = 'contenttypes';
  const PARAM__PUBLISHERS = 'publishers';
  const PARAM__PROVIDERS = 'providers';
  const PARAM__COPYRIGHTS = 'copyrights';
  const PARAM__COMPETENCIES = 'competencies';

  /**
   * Common WSS settings
   */
  const SETTING__PERSISTENT_SETTINGS = 'wss_base_settings__persistent';

  /**
   * Define the life time of cached responses.
   *
   * @var type
   */
  public static $cacheExpirationTime = 1800;

  /**
   * Associative array containing setting identifiers and default values.
   *
   * @var array
   */
  protected static $settings;

  /**
   * An array defining request parameter mappings.
   *
   * This array should be filled dynamically, because of contained display names
   * for parameters, which should be given using the translation function `t()`.
   *
   * @var array
   */
  protected $parameterMappings = array();

  /**
   * The actual settings.
   *
   * @var array
   */
  protected static $data = array();

  /**
   * Class instance.
   *
   * @var \publicplan\wss\base\SearchService[]
   */
  protected static $instance = array();

  /**
   * Protected constructor.
   *
   * Initialize settings.
   *
   * @throws \Exception
   */
  protected function __construct() {
    $class_name = get_called_class();
    self::$settings = $class_name::getDefaultSettings();
    if (!isset(self::$settings)) {
      throw new \Exception(t('%class is missing settings.', array('%class' => $class_name)));
    }

    foreach (self::$settings as $name => $default_value) {
      if (isset($default_value)) {
        continue;
      }

      self::$data[$name] = array(
        'value' => variable_get($name, $default_value),
        'sync' => TRUE,
      );

      if (!isset(self::$data[$name]['value'])) {
        watchdog('wss_base', "Missing required setting '%setting'.", array('%setting' => $name), WATCHDOG_ERROR);
      }
    }
  }

  /**
   * Destructor.
   *
   * Automatically save current settings.
   */
  public function __destruct() {
    $this->save();
  }

  /**
   * Initialization.
   *
   * Internally used as first call in all static methods, to verify that all
   * attributes are initialized.
   */
  public static function init() {
    $class_name = get_called_class();
    if (!isset(self::$instance[$class_name])) {
      self::$instance[$class_name] = new $class_name();
    }

    return self::$instance[$class_name];
  }

  /**
   * Returns all settings as associative array.
   *
   * @return array
   *   Associative settings array.
   */
  public static function getSettings() {
    self::init();
    $settings = array();
    foreach (self::$settings as $name => $default_value) {
      if (isset(self::$data[$name])) {
        $settings[$name] = self::$data[$name]['value'];
      }

      $settings[$name] = self::loadSetting($name);
    }

    return $settings;
  }

  /**
   * Saves multiple settings at once.
   *
   * @param array $settings
   *   Associative array containing multiple settings.
   *
   * @return \publicplan\wss\base\SearchService
   *   Returns itself.
   * @throws \RuntimeException
   */
  public static function setSettings($settings) {
    foreach ($settings as $name => $value) {
      self::setSetting($name, $value);
    }

    return self::$instance;
  }

  /**
   * Returns the specified setting's value.
   *
   * @param string $setting
   *   Variable/Setting identifier.
   *
   * @return mixed
   *   Queried setting value.
   * @throws \RuntimeException
   */
  public static function getSetting($setting) {
    self::init();
    if (!isset(self::$data[$setting])) {
      return self::loadSetting($setting);
    }

    return self::$data[$setting]['value'];
  }

  /**
   * Save a setting.
   *
   * @param string $setting
   *   Setting name.
   * @param mixed $value
   *   Value to set.
   *
   * @return \publicplan\wss\base\SearchService
   *   Returns itself.
   * @throws \RuntimeException
   */
  public static function setSetting($setting, $value) {
    self::$data[$setting] = array(
      'value' => $value,
      'sync' => FALSE,
    );

    return self::instance;
  }

  /**
   * Load setting from database.
   *
   * This method doesn't care if the variable has already been loaded or about
   * its sync status.
   *
   * @param string $setting
   *   Setting key to load value of.
   *
   * @return mixed
   *   Queried setting value.
   */
  public static function loadSetting($setting) {
    self::init();
    self::$data[$setting] = array(
      'value' => variable_get($setting, isset(self::$settings[$setting]) ? self::$settings[$setting] : NULL),
      'sync' => TRUE,
    );

    return self::$data[$setting]['value'];
  }

  /**
   * Associative array containing setting identifiers and default values.
   *
   * Array keys are internally used with Drupal's variable_*() functions.
   *
   * @return array
   */
  public static function getDefaultSettings() {
    return array(
      self::SETTING__PERSISTENT_SETTINGS => TRUE,
    );
  }

  /**
   * Store current settings to database.
   *
   * @return \publicplan\wss\base\SearchService
   *   Returns itself.
   */
  public static function save() {
    self::init();
    foreach (self::$data as $name => &$setting) {
      if ($setting['sync']) {
        continue;
      }

      variable_set($name, $setting['value']);
      $setting['sync'] = TRUE;
    }

    return self::$instance;
  }

  /**
   * Returns the expiration time (UNIX timestamp) for a new cache entry.
   *
   * @return int
   *   UNIX Timestamp.
   */
  public static function getCacheExpirationTime() {
    return time() + static::$cacheExpirationTime;
  }

  /**
   * Returns an array for request parameter mapping.
   *
   * @return array
   *   An array defining mappings for available request parameters.
   */
  public function getParameterMappings() {
    return $this->parameterMappings;
  }
}
