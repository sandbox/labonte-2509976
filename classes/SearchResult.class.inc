<?php // $id:$

/**
 * @file
 * This file contains the SearchResult class implementing the SearchResult
 * interface.
 */

namespace publicplan\wss\base;

abstract class SearchResult implements ResultInterface {
  /**
   * Service identifier.
   *
   * @var string
   */
  protected $service;

  /**
   * Raw/Native input data.
   *
   * Might be the array representation of a JSON string or ArrayObject
   * representation of an XML, rather than the real raw string.
   *
   * @var mixed
   */
  protected $raw = NULL;

  /**
   * Renderable result data.
   *
   * @var array
   */
  protected $data = array();

  /**
   * Object instances for clone based object initialization.
   *
   * @var \publicplan\wss\base\SearchResult[]
   */
  protected static $objects = array();

  /**
   * Protected constructor.
   *
   * Use static create() function for initialization of the objects.
   */
  protected function __construct() {}

  /**
   * Initialize a new object of this class.
   *
   * @param bool $clean
   *   Whether to reset or keep existing values of the clone base.
   *
   * @return \publicplan\wss\base\SearchResult
   *   New (optinonally clean) object.
   */
  public static function create($clean = TRUE) {
    $class_name = get_called_class();
    if (!isset(self::$objects[$class_name])) {
      self::$objects[$class_name] = new $class_name();
    }

    $new_object = clone self::$objects[$class_name];
    if ($clean) {
      $new_object->clean();
    }

    return $new_object;
  }

  /**
   * Drop all resource object specific data.
   *
   * @return \publicplan\wss\base\SearchResult
   *   Returns itself.
   */
  public function clean() {
    $this->raw = NULL;
    $this->data = array();

    return $this;
  }

  /**
   * Parse result data.
   *
   * @param mixed $raw_data
   *   Raw result data.
   *
   * @return \publicplan\wss\base\SearchResult
   *   Returns itself.
   */
  abstract public function parse($raw_data);

  /**
   * Returns the service specific identifier of the result/resource.
   *
   * @return string
   *   Identifier string of the appropriate underlying service.
   */
  abstract public function getServiceId();

  /**
   * Returns the external URL of the requested resource.
   *
   * @return string
   *   The external resource URL.
   */
  abstract public function getResourceLocation();

  /**
   * Return the data array.
   *
   * @return array
   *   Array containing the parsed result data.
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Set corresponding response object.
   *
   * @param \publicplan\wss\base\SearchResponse $response
   *   Corresponding response object.
   *
   * @return \publicplan\wss\base\SearchResult
   *   Returns itself.
   */
  public function setResponse(&$response) {
    $this->response = $response;

    return $this;
  }

  /**
   * Implements ArrayIterator interface.
   *
   * @return \ArrayIterator
   *   ArrayIterator over data array.
   */
  public function getIterator() {
    return new \ArrayIterator($this->data);
  }

  /**
   * Implements ArrayAccess interface.
   *
   * @param mixed $offset
   *   Index/key to check for existence.
   *
   * @return bool
   *   TRUE if the given offset/index/key exists.
   */
  public function offsetExists($offset) {
    return array_key_exists($offset, $this->data);
  }

  /**
   * Implements ArrayAccess interface.
   *
   * @param mixed $offset
   *   Index/key for which to return the value.
   *
   * @return mixed
   *   Data value.
   */
  public function offsetGet($offset) {
    return $this->data[$offset];
  }

  /**
   * Implements ArrayAccess interface.
   *
   * @param mixed $offset
   *   Index/key to set value for.
   * @param mixed $value
   *   Value to be set.
   */
  public function offsetSet($offset, $value) {
    $this->data[$offset] = $value;
  }

  /**
   * Implements ArrayAccess interface.
   *
   * @param mixed $offset
   *   Index/key of the value, that should be unset.
   */
  public function offsetUnset($offset) {
    unset($this->data[$offset]);
  }
}
