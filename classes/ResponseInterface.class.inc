<?php // $Id$
/**
 * @file
 * This file contains the SearchResponse class' interface.
 */

namespace publicplan\wss\base;

interface ResponseInterface extends \IteratorAggregate, \ArrayAccess, \Countable {

  /**
   * Returns an array containing all results.
   *
   * Array keys are the appropriate result IDs.
   *
   * @return \SearchResult[]
   *   The actual results.
   */
  public function getResults();

  /**
   * Returns the result specified by its index.
   *
   * @param string $index
   *   Result ID.
   *
   * @return \SearchResult
   *   The search result with ID $index.
   */
  public function getResult($index);

  /**
   * Get the originating HTTP request as raw string.
   *
   * @return string
   *   The actual request string.
   */
  public function getRequestString();

  /**
   * Get HTTP response code.
   *
   * @return int
   *   Http status code of the most recent response.
   */
  public function getStatusCode();

  /**
   * Get the HTTP status message.
   *
   * @return string
   *   Http status message of the most recent response.
   */
  public function getStatusMessage();

  /**
   * Get HTTP error message or FALSE if no error occurred.
   *
   * @return string|FALSE
   *   Http error message or FALSE if the most recent request succeeded.
   */
  public function getError();

  /**
   * Get the HTTP version string e.g. 'HTTP/1.1'.
   *
   * @return string|FALSE
   *   Http version string.
   */
  public function getVersion();

  /**
   * Get the initial HTTP status code, if the request was redirected.
   *
   * @return int|FALSE
   *   The original http status code before redirect.
   */
  public function getRedirectStatusCode();

  /**
   * Get the target's URL string in case of a redirect; otherwise FALSE.
   *
   * @return string|FALSE
   *   Value of the 'Location' response header or FALSE.
   */
  public function getRedirectUrl();

  /**
   * Get the HTTP response headers as associative array.
   *
   * @return array|FALSE
   *   Response headers as associative array or FALSE.
   */
  public function getHeaders();

  /**
   * Returns the raw response body.
   *
   * @return string|FALSE
   *   Response body as string or FALSE.
   */
  public function getRawData();

  /**
   * Returns an appropriate PHP representation for response data.
   *
   * Data format depends on the service and should be in an appropriate PHP
   * representation for the raw response format.
   * (eg. JSON -> Array/ArrayObject, XML -> SimpleXMLElement)
   *
   * @return mixed
   *   Response body in PHP representation.
   */
  public function getData();

  /**
   * Returns last received/built instance of Response class.
   *
   * @return \publicplan\wss\base\SearchResponse
   *   Return the most recent object of this class.
   */
  public static function getLast();
}
