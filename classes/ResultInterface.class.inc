<?php
/**
 * @file
 * This file contains the SearchResult class interface.
 */

namespace publicplan\wss\base;

interface ResultInterface extends \ArrayAccess, \IteratorAggregate {

  /**
   * Initialize a new object of this class.
   *
   * @param bool $clean
   *   Whether to reset or keep existing values of the clone base.
   *
   * @return \publicplan\wss\base\SearchResult
   *   New (optinonally clean) object.
   */
  public static function create($clean);

  /**
   * Drop all resource object specific data.
   *
   * @return \publicplan\wss\base\SearchResult
   *   Returns itself.
   */
  public function clean();

  /**
   * Return the results data array.
   *
   * @return array
   *   Parsed data array.
   */
  public function getData();
}
