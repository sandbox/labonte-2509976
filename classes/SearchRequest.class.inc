<?php
/**
 * @file
 * This file contains an abstract Request class implementation.
 */

namespace publicplan\wss\base;

abstract class SearchRequest implements RequestInterface   {
  /**
   * Instance of Request class, used in self::create() for cloning.
   *
   * @var SearchRequest
   */
  protected static $instance = array();

  /**
   * Service identifier.
   *
   * @var string
   */
  protected $service;

  /**
   * Request target/URL.
   *
   * @var string
   */
  protected $url = '';

  /**
   * Request method.
   *
   * @var self::GET|self::POST
   */
  protected $method;

  /**
   * Request headers as associative array.
   *
   * @var array|FALSE
   */
  public $headers = FALSE;

  /**
   * Request body.
   *
   * @var string
   */
  protected $body = '';

  /**
   * Request body as associatve array, but only if data was derived through
   * $this->setData().
   *
   * @var array|FALSE
   */
  protected $bodyData = FALSE;

  /**
   * Request execution timeout
   *
   * @var float
   */
  protected $timeout = self::DEFAULT__EXEC_TIMEOUT;

  /**
   * The maximum number of accepted redirects.
   *
   * @var int
   */
  protected $maxRedirects = self::DEFAULT__MAX_REDIRECTS;

  /**
   * The stream context resource, if set.
   *
   * @var resource|NULL
   */
  protected $context = FALSE;

  /**
   * The corresponding Response object.
   *
   * @var \publicplan\wss\base\http\Response|FALSE
   */
  protected $response = FALSE;

  /**
   * Request parameters.
   *
   * @var string
   */
  protected $parameters = array();

  /**
   * Returns a new Request.
   *
   * @return SearchRequest
   *   Returns itself.
   *
   * @deprecated
   */
  public static function create() {
    $class = get_called_class();
    if (!isset(self::$instance[$class])) {
      self::$instance[$class] = new $class();
      return self::$instance[$class];
    }

    $new = clone self::$instance[$class];
    return $new->reset();
  }

  /**
   * Wrapper
   *
   * @param bool $return_latest
   *   Set to false to get a clean/new object instance.
   *
   * @return SearchRequest
   *   Returns a SearchRequest object.
   */
  public static function get($return_latest = TRUE) {
    $class = get_called_class();
    if (!isset(self::$instance[$class])) {
      self::$instance[$class] = new $class();
      return self::$instance[$class];
    }

    if ($return_latest && isset(self::$instance[$class])) {
      return self::$instance[$class];
    }
    else {
      $new = clone self::$instance[$class];
      return $new->reset();
    }
  }

  /**
   * Unsets all values of the current request.
   *
   * @return SearchRequest
   *   Returns itself.
   */
  public function reset() {
    $this->body = $this->url = '';
    $this->bodyData = $this->context = $this->headers = FALSE;
    return $this->setMethod()->setMaxRedirects()->setTimeout();
  }

  /**
   * Returns the stream context resource if set, otherwise FALSE.
   *
   * @return resource|FALSE
   *   The stream context resource reference.
   */
  public function getStreamContext() {
    return $this->context;
  }

  /**
   * Sets a resource context.
   *
   * A resource context can be produced using PHP's native function
   * stream_context_create().
   *
   * @param resource $context
   *   A stream context resource.
   *
   * @return SearchRequest
   *   Returns itself.
   */
  public function setStreamContext($context) {
    $this->context = $context;
    return $this;
  }

  /**
   * Returns the URL to which the request will be staged.
   *
   * @return string|FALSE
   *   URL string.
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * Set the request target URL.
   *
   * @param string $url
   *   URL to which to stage the request.
   *
   * @return SearchRequest
   *   Returns itself.
   */
  public function setUrl($url) {
    assert('is_string($url)');
    $this->url = $url;
    return $this;
  }

  /**
   * Set a parameter value.
   *
   * @param string $param
   *   Parameter name.
   * @param mixed $value
   *   Parameter value to set.
   *
   * @return \publicplan\wss\base\SearchRequest
   *   Return itself.
   */
  public function setParameter($param, $value) {
    $this->parameters[$param] = $value;

    return $this;
  }

  /**
   * Set a parameter array.
   *
   * Previous set or existing default parameters are beeing joint with the one
   * given as `$params`.
   *
   * @param array $params
   *   Parameter array.
   *
   * @return \publicplan\wss\base\SearchRequest
   *   Returns itself.
   */
  public function setParameters($params) {
    $this->parameters = array_merge($this->parameters, $params);

    return $this;
  }

  /**
   * Get a parameter's value.
   *
   * @param string $param
   *   Parameter name.
   *
   * @return mixed
   *   Return parameter value or NULL if it is not set.
   */
  public function getParameter($param) {
    if (!isset($this->parameters[$param])) {
      return NULL;
    }

    return $this->parameters[$param];
  }

  /**
   * Return the whole parameter array.
   *
   * @return array
   *   Parameter array.
   */
  public function getParameters() {
    return $this->parameters;
  }

  /**
   * Get the HTTP response headers as associative array.
   *
   * @return array|FALSE
   *   Associative array of response headers.
   */
  public function getHeaders() {
    return $this->headers;
  }

  /**
   * Set the HTTP headers as associative array.
   *
   * @param array $headers
   *   Associative array containing the request headers.
   *
   * @return SearchRequest
   *   Returns itself.
   */
  public function setHeaders($headers) {
    assert('is_array($headers)');
    $this->headers = $headers;
    return $this;
  }

  /**
   * Returns the request method.
   *
   * @return string
   *   Request method (GET/POST/...)
   */
  public function getMethod() {
    return $this->method;
  }

  /**
   * Sets the request method to use.
   *
   * @param string $method
   *   Either self::GET or self::POST.
   *
   * @return SearchRequest
   *   Returns itself.
   */
  public function setMethod($method = self::METHOD__GET) {
    assert('$method === \publicplan\wss\base\SearchRequest::METHOD__GET || ' .
        '$method === SearchRequest::METHOD__POST');
    $this->method = $method;
    return $this;
  }

  /**
   * Returns the maximum execution time of the request.
   *
   * @return float
   *   Configured max exection time for a request.
   */
  public function getTimeout() {
    return (float) $this->timeout;
  }

  /**
   * Set the request timeout.
   *
   * This timeout takes effect no matter if a response is received or not: When
   * the timeout exceeds, the request is canceled.
   *
   * @param float $seconds
   *   Overall request Timeout.
   *
   * @return SearchRequest
   *   Returns itself.
   */
  public function setTimeout($seconds = self::DEFAULT__EXEC_TIMEOUT) {
    assert('is_numeric($seconds)');
    $this->timeout = (float) $seconds;
    return $this;
  }

  /**
   * Returns the maximum number of redirects accepted.
   *
   * @return int
   *   Maximum number of accepted redirects before the requests aborts.
   */
  public function getMaxRedirects() {
    return (int) $this->maxRedirects;
  }

  /**
   * Defines the maximum acceptable count of redirects.
   *
   * @param int $count
   *   Number of accepted redirects.
   *
   * @return SearchRequest
   *   Returns itself.
   */
  public function setMaxRedirects($count = self::DEFAULT__MAX_REDIRECTS) {
    assert('is_numeric($count)');
    $this->maxRedirects = (int) $count;
    return $this;
  }

  /**
   * Returns the resquest data/body.
   *
   * @return array|FALSE
   *   Request body data if set.
   */
  public function getData() {
    return $this->bodyData;
  }

  /**
   * Set request data based on an associative array structure.
   *
   * Takes an associative array, puts it into URL-encoded format and sets the
   * result as request body.
   *
   * @return SearchRequest
   *   Returns itself.
   */
  public function setData($data) {
    assert('is_array($data)');

    $this->body = $this->urlEncode($this->bodyData = $data);
    return $this;
  }

  /**
   * Returns the raw request body.
   *
   * @return string|FALSE
   *   The actual request body.
   */
  public function getRawData() {
    return $this->body;
  }

  /**
   * Takes data as it is for the request body.
   *
   * @return SearchRequest
   *   Returns itself.
   */
  public function setRawData($data) {
    assert('is_string($data)');
    $this->body = $data;
    return $this;
  }

  /**
   * Return the most recent Response object if applicable.
   *
   * @return \publicplan\wss\base\SearchResponse
   *   Response object.
   */
  public function getResponse() {
    return $this->response;
  }

  /**
   * Returns an array containing all results.
   *
   * Array keys are the appropriate result IDs.
   *
   * @return \publicplan\wss\base\SearchResult[]
   *   The actual results.
   */
  public function getResults() {
    return $this->response->getResults();
  }

  /**
   * Returns the result specified by its index.
   *
   * @param string $index
   *   Result ID.
   *
   * @return \publicplan\wss\base\SearchResult
   *   The search result with ID $index.
   */
  public function getResult($index) {
    return $this->response->getResult($index);
  }

  /**
   * Sends request and receives response.
   *
   * @param bool $use_cache
   *   Set to false to avoid using the drupal cache.
   * @param bool $parse_response
   *   Set to false to return the plain drupal response object.
   * @return SearchRequest|stdClass
   *   Returns itself.
   */
  public function send($use_cache = TRUE, $parse_response = TRUE) {
    $class_name = get_class($this);
    // Check whether to use the cache:
    if (defined("$class_name::CACHE") && $use_cache) {
      $cid = $this->getCid();
      $cache_bin = constant("$class_name::CACHE");
    }
    // Try loading cached response:
    if (isset($cid) && ($cached_response = cache_get($cid, $cache_bin))) {
      $raw_response = $cached_response->data;
    }
    // Stage a real request:
    else {
      $raw_response = drupal_http_request($this->getUrl(), $this->getOptions());
      if (in_array(substr($raw_response->code, 0, 1), array(2, 3))) {
        if (isset($cid) && isset($cache_bin)) {
          cache_set($cid, $raw_response, $cache_bin, $this->getService()->getCacheExpirationTime());
        }
      }
      else {
        throw new \RuntimeException(t('@service returned error code @code: %msg', array(
            '@service' => $this->service,
            '@code' => $raw_response->code,
            '%msg' => $raw_response->error,
          )), $raw_response->code
        );
      }
    }

    if (!$parse_response) {
      return $raw_response;
    }

    $response_class = '\publicplan\wss\\' . $this->service . '\Response';
    $this->response = call_user_func(array($response_class, 'create'));
    $this->response->parse($this, $raw_response);

    return $this;
  }

  /**
   * Return the appropriate Service object.
   *
   * @return \publicplan\wss\base\SearchService
   *   Returns the corresponding Service class object.
   */
  public function getService() {
    $service_class_name = str_replace('Request', 'Service', get_class($this));
    return $service_class_name::init();
  }

  /**
   * Build an options array for use with drupal_http_request().
   *
   * Options are based on the current object's attributes.
   *
   * @return array
   *   drupal_http_request() options array.
   */
  protected function getOptions() {
    $options = array(
      'max_redirects' => $this->maxRedirects,
      'method' => $this->method,
      'timeout' => $this->timeout,
    );

    if ($this->body) {
      $options['data'] = $this->body;
    }
    if ($this->context) {
      $options['context'] = $this->context;
    }
    if ($this->headers) {
      $options['headers'] = $this->headers;
    }

    return $options;
  }

  /**
   * URL-encodes an associative array.
   *
   * @param array $data
   *   Associative (optional nested) array to encode.
   *
   * @return string
   *   URL-encoded query string.
   */
  public static function urlEncode($data) {
    $traverse = null;
    $parts = array();
    $traverse = function ($data_portion, $parent = '') use ($traverse, &$parts) {
      foreach ($data_portion as $key => $value) {
        $name = empty($parent) ? urlencode($key) : ($parent . '['  . (!is_int($key) ? urlencode($key) : '') .  ']');
        if (is_array($value)) {
          $traverse($value, $name);
        }
        elseif (is_string($value)) {
          $parts[] = $name . '=' . urlencode(\filter_xss($value));
        }
      }
    };

    $traverse($data);

    return implode('&', $parts);
  }

  /**
   * URL-encode a single key/value pair.
   *
   * Examples:
   *      &order=asc
   *  or  &keywords[]=foo&keywords[]=bar
   *
   * @param string $key
   *   Parameter key.
   * @param string|array $value
   *   Parameter value.
   *
   * @return string
   *   Query parameters.
   */
  public static function urlEncodeComponent($key, $value) {
    $query_component = '';
    if (is_array($value)) {
      $component_array = array();
      foreach ($value as $val) {
        if (!empty($val)) {
          $component_array[] = $key . '[]=' . urlencode(\filter_xss(urldecode($val)));
        }
      }
      $query_component .= implode('&', $component_array);
    }
    else {
      $query_component .= $key . '=' . urlencode(\filter_xss(html_entity_decode(urldecode($value))));
    }

    return $query_component;
  }

  /**
   * Return a hash based on URL and body data as CID (Cache ID).
   *
   * @return string
   *   Return a string for use as CID for the specific request.
   */
  public function getCid() {
    return md5(serialize($this->url) . serialize($this->body));
  }
}
