<?php // $Id$
/**
 * @file
 * This file contains the module's autoloader and registers it with
 * spl_autoload_register().
 */

/**
 * Autoloader to register with spl_autoload_register().
 *
 * This autoloader handles autoloading for all subsequent wss modules.
 *
 * @param string $class
 *   PHP class for which to try autoloading
 */
function _wss_base_autoload($class) {
  $class_path = explode('\\', $class);
  if (implode('\\', array_slice($class_path, 0, 2)) === 'publicplan\wss') {
    $module = 'wss_' . (isset($class_path[2]) ? $class_path[2] : 'base');
    $module_dir = drupal_get_path('module', $module);
    $relative_path = implode(DIRECTORY_SEPARATOR, array_slice($class_path, 3));
    $file = $module_dir . DIRECTORY_SEPARATOR . 'classes' .
      DIRECTORY_SEPARATOR . $relative_path . '.class.inc';
    if (is_file($file) && is_readable($file)) {
      include_once $file;
    }
  }
}

spl_autoload_register('_wss_base_autoload', '.class.inc');
